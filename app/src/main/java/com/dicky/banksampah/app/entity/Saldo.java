package com.dicky.banksampah.app.entity;

/**
 * Kelas ini adalah kelas yang merepresentasikan atau menggambarkan data dari tabel saldo di database
 */
public class Saldo {

    //variabel id untuk menampung data id
    private int id;
    //variabel user untuk menampung data user
    private User user;
    //variabel totalBayar untuk menampung data total bayar yang dipunyai user
    private int totalBayar;
    //variabel totalBayar untuk menampung data total berat sampah yang dipunyai user
    private int totalSampah;

    //constructor untuk inisiasi kelas
    public Saldo() {
    }

    //constructor untuk inisiasi kelas dengan parameter id
    public Saldo(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel id
    public int getId() {
        return id;
    }

    //method untuk memberikan value ke variabel id
    public void setId(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel user
    public User getUser() {
        return user;
    }

    //method untuk memberikan value ke variabel user
    public void setUser(User user) {
        this.user = user;
    }

    //method untuk mengambil value dari variabel totalBayar
    public int getTotalBayar() {
        return totalBayar;
    }

    //method untuk memberikan value ke variabel totalBayar
    public void setTotalBayar(int totalBayar) {
        this.totalBayar = totalBayar;
    }

    //method untuk mengambil value dari variabel totalSampah
    public int getTotalSampah() {
        return totalSampah;
    }

    //method untuk mengambil value dari variabel totalSampah
    public void setTotalSampah(int totalSampah) {
        this.totalSampah = totalSampah;
    }

    //method toString untuk mencetak model ini kedalam bentuk string (plain text)
    @Override
    public String toString() {
        return "Saldo{" +
                "id=" + id +
                ", user=" + user +
                ", totalBayar=" + totalBayar +
                ", totalSampah=" + totalSampah +
                '}';
    }
}
