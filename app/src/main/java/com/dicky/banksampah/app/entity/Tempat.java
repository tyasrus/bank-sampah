package com.dicky.banksampah.app.entity;

import android.os.Parcel;
import android.os.Parcelable;
/**
 * Kelas ini adalah kelas yang merepresentasikan atau menggambarkan data dari tabel tempat di database
 */
public class Tempat implements Parcelable {

    //variabel id untuk menampung data id
    private int id;
    //variabel user untuk menampung data user
    private User user;
    //variabel nama untuk menampung nama
    private String nama;
    //variabel alamat untuk menampung alamat
    private String alamat;
    //variabel latitude untuk menampung latitude
    private double latitude;
    //variabel longitude untuk menampung longitude
    private double longitude;

    //constructor untuk inisiasi kelas
    public Tempat() {
    }

    //constructor untuk inisiasi kelas dengan parameter id
    public Tempat(int id) {
        this.id = id;
    }

    protected Tempat(Parcel in) {
        id = in.readInt();
        user = in.readParcelable(User.class.getClassLoader());
        nama = in.readString();
        alamat = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<Tempat> CREATOR = new Creator<Tempat>() {
        @Override
        public Tempat createFromParcel(Parcel in) {
            return new Tempat(in);
        }

        @Override
        public Tempat[] newArray(int size) {
            return new Tempat[size];
        }
    };

    //method untuk mengambil value dari variabel id
    public int getId() {
        return id;
    }

    //method untuk memberikan value ke variabel id
    public void setId(int id) {
        this.id = id;
    }

    //method untuk mengambil value dari variabel user
    public User getUser() {
        return user;
    }

    //method untuk memberikan value ke variabel user
    public void setUser(User user) {
        this.user = user;
    }

    //method untuk mengambil value dari variabel nama
    public String getNama() {
        return nama;
    }

    //method untuk memberikan value ke variabel nama
    public void setNama(String nama) {
        this.nama = nama;
    }

    //method untuk mengambil value dari variabel alamat
    public String getAlamat() {
        return alamat;
    }

    //method untuk memberikan value ke variabel alamat
    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    //method untuk mengambil value dari variabel latitude
    public double getLatitude() {
        return latitude;
    }

    //method untuk memberikan value ke variabel latitude
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    //method untuk mengambil value dari variabel longitude
    public double getLongitude() {
        return longitude;
    }

    //method untuk memberikan value ke variabel longitude
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    //method toString untuk mencetak model ini kedalam bentuk string (plain text)
    @Override
    public String toString() {
        return "Tempat{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", user='" + user + '\'' +
                ", alamat='" + alamat + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeParcelable(user, flags);
        dest.writeString(nama);
        dest.writeString(alamat);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }
}
