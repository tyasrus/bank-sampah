package com.dicky.banksampah.app.entity;

import java.util.Date;
/**
 * Kelas ini adalah kelas yang merepresentasikan atau menggambarkan data dari tabel transaksi di database
 */
public class Transaksi {

    private int id;
    private User user;
    private Tempat tempat;
    private Date waktu;
    private int totalHarga;
    private int totalSampah;
    private boolean statusBayar;

    public Transaksi() {
    }

    public Transaksi(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Tempat getTempat() {
        return tempat;
    }

    public void setTempat(Tempat tempat) {
        this.tempat = tempat;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public int getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(int totalHarga) {
        this.totalHarga = totalHarga;
    }

    public int getTotalSampah() {
        return totalSampah;
    }

    public void setTotalSampah(int totalSampah) {
        this.totalSampah = totalSampah;
    }

    public boolean isStatusBayar() {
        return statusBayar;
    }

    public void setStatusBayar(boolean statusBayar) {
        this.statusBayar = statusBayar;
    }

    @Override
    public String toString() {
        return "Transaksi{" +
                "id=" + id +
                ", user=" + user +
                ", tempat=" + tempat +
                ", waktu=" + waktu +
                ", totalHarga=" + totalHarga +
                ", totalSampah=" + totalSampah +
                ", statusBayar=" + statusBayar +
                '}';
    }
}
