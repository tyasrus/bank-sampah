package com.dicky.banksampah.app.entity;

/**
 * Kelas ini adalah kelas yang merepresentasikan atau menggambarkan data dari tabel user_level di database
 */
public class UserLevel {

    private int id;
    private String nama;

    public UserLevel() {
    }

    public UserLevel(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "UserLevel{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                '}';
    }
}
