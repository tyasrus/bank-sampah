package com.dicky.banksampah.app.service;

import android.os.Handler;
import android.os.Looper;

import com.dicky.banksampah.app.entity.Saldo;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.config.Api;
import com.dicky.banksampah.app.service.config.BaseView;

import okhttp3.FormBody;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Kelas ini digunakan untuk menangani transaksi (CRUD) ke atau dari tabel saldo
 */
public class SaldoService {

  private static SaldoService instance;
  private OkHttpClient client = new OkHttpClient();
  private RequestBody body;
  private Request request;

  public static SaldoService getInstance() {
    if (instance == null) {
      instance = new SaldoService();
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }

    return instance;
  }

  public void get(final BaseView<Saldo> view, Saldo saldo) {
    String query = Api.BASE_URL + Api.SALDO_GET;
    query = saldo.getId() > 0 ? query + "?id=" + saldo.getId()
        : saldo.getUser().getId() > 0 ? query + "?id_user=" + saldo.getUser().getId()
            : saldo.getId() > 0 && saldo.getUser().getId() > 0 ? query
                + "?id="
                + saldo.getId()
                + "&id_user="
                + saldo.getUser().getId() : "";
    request = new Request.Builder().url(query).get().build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  Saldo result = new Saldo();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray.length() > 0) {
                    JSONObject dataObject = jsonArray.getJSONObject(0);
                    result.setTotalBayar(dataObject != null ? dataObject.getInt("total_harga") : 0);
                    result.setTotalSampah(
                        dataObject != null ? dataObject.getInt("total_sampah") : 0);
                  }

                  view.onSuccess(result, jsonObject.getString("status"));
                } else {
                  view.onFailed("Data tidak ditemukan");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Data tidak ditemukan");
              }
            }
          }
        });
      }
    });
  }

  public void delete(final DeleteView view, User user) {
    body = new FormBody.Builder().add("id_user", String.valueOf(user.getId())).build();

    request = new Request.Builder().url(Api.BASE_URL + Api.SALDO_DELETE).post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onError(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess();
                } else {
                  view.onError("Terjadi kesalahan saat menghapus");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onError("Terjadi kesalahan saat menghapus");
              }
            }
          }
        });
      }
    });
  }

  public interface DeleteView {
    void onSuccess();

    void onError(String message);
  }
}
