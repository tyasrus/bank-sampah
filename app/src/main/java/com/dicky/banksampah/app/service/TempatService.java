package com.dicky.banksampah.app.service;

import android.os.Handler;
import android.os.Looper;

import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.service.config.Api;
import com.dicky.banksampah.app.service.config.BaseView;

import com.dicky.banksampah.app.service.config.Message;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/**
 * Kelas ini digunakan untuk menangani transaksi (CRUD) ke atau dari tabel tempat
 */
public class TempatService {

    private static TempatService instance;
    private OkHttpClient client = new OkHttpClient();
    private RequestBody body;
    private Request request;

    public static TempatService getInstance() {
        if (instance == null) {
            instance = new TempatService();
            //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            //baris ini -> statement untuk set level dari log ini
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            //baris ini -> statement untuk inisiasi variabel client dengan interceptor
            instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        }
        return instance;
    }

    public void save(final BaseView<Tempat> view, Tempat tempat) {
        body = new FormBody.Builder()
                .add("id_user", String.valueOf(tempat.getUser().getId()))
                .add("name", tempat.getNama())
                .add("alamat", tempat.getAlamat())
                .add("latitude", String.valueOf(tempat.getLatitude()))
                .add("longitude", String.valueOf(tempat.getLongitude()))
                .build();

        //request ini untuk menjalankan servis http ke server
        request = new Request.Builder()
                .url(Api.BASE_URL + Api.TEMPAT_SAVE)
                //http request post
                .post(body)
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    view.onSuccess(null, jsonObject.getString("status"));
                                } else {
                                    view.onFailed("Terjadi kesalahan saat menyimpan");
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed("Terjadi kesalahan saat menyimpan");
                            }
                        }
                    }
                });
            }
        });
    }

    public void get(final GetView view, Tempat tempat) {
        String query = Api.BASE_URL + Api.TEMPAT_GET;
        query = tempat.getId() > 0 ? query + "?id=" + tempat.getId() :
                tempat.getUser().getId() > 0 ? query + "?id_user=" + tempat.getUser().getId() :
                        tempat.getId() > 0 && tempat.getUser().getId() > 0 ? query + "?id=" + tempat.getId() + "&id_user=" + tempat.getUser().getId() : "";
        request = new Request.Builder()
                .url(query)
                .get()
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    Tempat result = new Tempat();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    JSONObject dataObject = jsonArray.getJSONObject(0);
                                    result.setId(dataObject.getInt("id"));
                                    result.setNama(dataObject.getString("name"));
                                    result.setAlamat(dataObject.getString("alamat"));
                                    result.setLatitude(dataObject.getDouble("latitude"));
                                    result.setLongitude(dataObject.getDouble("longitude"));
                                    view.onSuccess(result, jsonObject.getString("status"));
                                } else {
                                    view.onFailed("Data tidak ditemukan");
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed("Data tidak ditemukan");
                            }
                        }
                    }
                });
            }
        });
    }

    public void getAll(final AllView view, Tempat tempat) {
        request = new Request.Builder()
                .url(Api.BASE_URL + Api.TEMPAT_ALL + "?id_user_level=" + tempat.getUser().getLevel().getId())
                .get()
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    List<Tempat> results = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject dataObject = jsonArray.getJSONObject(i);
                                            Tempat tempat = new Tempat();
                                            tempat.setId(dataObject.getInt("id"));
                                            tempat.setNama(dataObject.getString("name"));
                                            tempat.setAlamat(dataObject.getString("alamat"));
                                            tempat.setLatitude(dataObject.getDouble("latitude"));
                                            tempat.setLongitude(dataObject.getDouble("longitude"));
                                            results.add(tempat);
                                        }
                                    }

                                    view.onSuccess(results,
                                        jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                                            : Message.LIST_RESULT_NOT_FOUND);
                                } else {
                                    view.onFailed(Message.LIST_RESULT_FAILED);
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed(Message.LIST_RESULT_FAILED);
                            }
                        }
                    }
                });
            }
        });
    }

    public void getNearbys(final AllView view, Tempat tempat) {
        request = new Request.Builder()
                .url(Api.BASE_URL + Api.TEMPAT_ALL + "?latitude=" + tempat.getLatitude() + "&longitude=" + tempat.getLongitude())
                .get()
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    List<Tempat> results = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    if (jsonArray != null) {
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject dataObject = jsonArray.getJSONObject(i);
                                            Tempat tempat = new Tempat();
                                            tempat.setId(dataObject.getInt("id"));
                                            tempat.setNama(dataObject.getString("name"));
                                            tempat.setAlamat(dataObject.getString("alamat"));
                                            tempat.setLatitude(dataObject.getDouble("latitude"));
                                            tempat.setLongitude(dataObject.getDouble("longitude"));
                                            results.add(tempat);
                                        }
                                    }

                                    view.onSuccess(results, jsonObject.getString("status"));
                                } else {
                                    view.onFailed("Data tidak ditemukan");
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed("Data tidak ditemukan");
                            }
                        }
                    }
                });
            }
        });
    }

    public void delete(final DeleteView view, Tempat tempat) {
        body = new FormBody.Builder()
                .add("id", String.valueOf(tempat.getId()))
                .build();

        request = new Request.Builder()
                .url(Api.BASE_URL + Api.TEMPAT_DELETE)
                .post(body)
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    view.onSuccess(jsonObject.getString("status"));
                                } else {
                                    view.onFailed("Terjadi kesalahan saat menghapus");
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed("Terjadi kesalahan saat menghapus");
                            }
                        }
                    }
                });
            }
        });
    }

    public void checking(final CheckingView view, Tempat tempat) {
        request = new Request.Builder()
                .url(Api.BASE_URL + Api.TEMPAT_CHECKING + "?id=" + tempat.getUser().getId())
                .get()
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    view.onSuccess(jsonArray.length() > 0 && jsonArray.getJSONObject(0).getInt("status_jemput") == 1 ? true : false, jsonObject.getString("status"));
                                } else {
                                    view.onFailed("Terjadi kesalahan saat cek penjemputan");
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed("Terjadi kesalahan saat cek penjemputan");
                            }
                        }
                    }
                });
            }
        });
    }

    public void update(final BaseView<Tempat> view, Tempat tempat) {
        body = new FormBody.Builder()
                .add("id", String.valueOf(tempat.getId()))
                .add("name", tempat.getNama())
                .add("alamat", tempat.getAlamat())
                .add("latitude", String.valueOf(tempat.getLatitude()))
                .add("longitude", String.valueOf(tempat.getLongitude()))
                .build();

        request = new Request.Builder()
                .url(Api.BASE_URL + Api.TEMPAT_UPDATE)
                .post(body)
                .build();

        //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        view.onFailed(e.getMessage());
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getString("status").equals("success")) {
                                    view.onSuccess(null, jsonObject.getString("status"));
                                } else {
                                    view.onFailed("Terjadi kesalahan saat mengupdate");
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                view.onFailed("Terjadi kesalahan saat mengupdate");
                            }
                        }
                    }
                });
            }
        });
    }

    public interface GetView {
        void onSuccess(Tempat tempat, String message);

        void onFailed(String message);
    }

    public interface DeleteView {
        void onSuccess(String message);

        void onFailed(String message);
    }

    public interface AllView extends BaseView<List<Tempat>> {
    }

    public interface CheckingView {
        void onSuccess(Boolean aBoolean, String message);

        void onFailed(String message);
    }
}
