package com.dicky.banksampah.app.service;

import android.os.Handler;
import android.os.Looper;

import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.entity.Transaksi;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.config.Api;
import com.dicky.banksampah.app.service.config.BaseView;
import com.dicky.banksampah.app.service.config.Message;
import com.dicky.banksampah.app.util.DateUtil;

import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/**
 * Kelas ini digunakan untuk menangani transaksi (CRUD) ke atau dari tabel transaksi
 */
public class TransaksiService {

  private static TransaksiService instance;
  private OkHttpClient client = new OkHttpClient();
  private RequestBody body;
  private Request request;

  public static TransaksiService getInstance() {
    if (instance == null) {
      instance = new TransaksiService();
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
    return instance;
  }

  public void save(final BaseView<Transaksi> view, Transaksi transaksi) {
    body = new FormBody.Builder().add("id_user", String.valueOf(transaksi.getUser().getId()))
        .add("id_tempat", String.valueOf(transaksi.getTempat().getId()))
        .add("total_harga", String.valueOf(transaksi.getTotalHarga()))
        .add("total_sampah", String.valueOf(transaksi.getTotalSampah()))
        //.add("status_bayar", String.valueOf(transaksi.isStatusBayar()))
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.TRANSAKSI_SAVE)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(null, jsonObject.getString("status"));
                } else {
                  view.onFailed("Terjadi kesalahan saat menyimpan");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Terjadi kesalahan saat menyimpan");
              }
            }
          }
        });
      }
    });
  }

  public void getAll(final AllView view, int id, int idUserLevel) {
    request = new Request.Builder().url(
        Api.BASE_URL + Api.TRANSAKSI_ALL + "?id=" + id + "&id_user_level=" + idUserLevel)
        .get()
        .build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  List<Transaksi> results = new ArrayList<>();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                      JSONObject dataObject = jsonArray.getJSONObject(i);
                      Transaksi transaksi = new Transaksi();
                      transaksi.setId(dataObject.getInt("id"));
                      transaksi.setUser(new User(dataObject.getInt("id_user")));
                      transaksi.setTempat(new Tempat(dataObject.getInt("id")));
                      transaksi.setWaktu(DateUtil.formatDate(dataObject.getString("waktu")));
                      transaksi.setTotalHarga(dataObject.getInt("total_harga"));
                      transaksi.setTotalSampah(dataObject.getInt("total_sampah"));
                      results.add(transaksi);
                    }
                  }

                  view.onSuccess(results,
                      jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                          : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Data tidak ditemukan");
              }
            }
          }
        });
      }
    });
  }

  public interface AllView extends BaseView<List<Transaksi>> {
  }
}
