package com.dicky.banksampah.app.service;

import android.os.Handler;
import android.os.Looper;

import com.dicky.banksampah.app.entity.UserLevel;
import com.dicky.banksampah.app.service.config.Api;

import com.dicky.banksampah.app.service.config.Message;
import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/**
 * Kelas ini digunakan untuk menangani transaksi (CRUD) ke atau dari tabel user_level
 */
public class UserLevelService {

  private static UserLevelService instance;
  private OkHttpClient client = new OkHttpClient();
  private RequestBody body;
  private Request request;

  public static UserLevelService getInstance() {
    if (instance == null) {
      instance = new UserLevelService();
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
    return instance;
  }

  //untuk menampilkan identitas user pada aplikasi ini
  public void getAll(final AllView view) {
    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_LEVEL_ALL).get()
        //http request post
        .build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  List<UserLevel> results = new ArrayList<>();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                      UserLevel result = new UserLevel();
                      JSONObject dataObject = jsonArray.getJSONObject(i);
                      result.setId(dataObject.getInt("id"));
                      result.setNama(dataObject.getString("name"));
                      results.add(result);
                    }
                  }

                  view.onSuccess(results,
                      jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                          : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Data tidak ditemukan");
              }
            }
          }
        });
      }
    });
  }

  public interface AllView {
    void onSuccess(List<UserLevel> userLevels, String message);

    void onFailed(String message);
  }
}
