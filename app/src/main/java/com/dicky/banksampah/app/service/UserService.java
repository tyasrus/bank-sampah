package com.dicky.banksampah.app.service;

import android.os.Handler;
import android.os.Looper;

import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.entity.UserLevel;
import com.dicky.banksampah.app.service.config.Api;
import com.dicky.banksampah.app.service.config.BaseView;
import com.dicky.banksampah.app.service.config.Message;

import okhttp3.logging.HttpLoggingInterceptor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
/**
 * Kelas ini digunakan untuk menangani transaksi (CRUD) ke atau dari tabel user
 */
public class UserService {

  private static UserService instance;
  private OkHttpClient client = new OkHttpClient();
  private RequestBody body;
  private Request request;

  public static UserService getInstance() {
    if (instance == null) {
      instance = new UserService();
      //baris ini -> statement untuk inisiasi HttpLoggingInterceptor untuk keperluan log data yang didapat dari server
      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      //baris ini -> statement untuk set level dari log ini
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      //baris ini -> statement untuk inisiasi variabel client dengan interceptor
      instance.client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
    return instance;
  }

  //untuk melakukan pengecekan user ke server
  public void login(final LoginView view, final User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("email", user.getEmail())
        .add("password", user.getPassword())
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_LOGIN)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  JSONObject dataObject = jsonArray.getJSONObject(0);
                  User result = new User();
                  result.setId(dataObject.getInt("id"));
                  result.setLevel(new UserLevel(dataObject.getInt("id_user_level")));
                  view.onSuccess(result, jsonObject.getString("status"));
                } else {
                  view.onFailed("Akun tidak valid");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Akun tidak valid");
              }
            }
          }
        });
      }
    });
  }

  //untuk melakukan pendaftaran user baru
  public void signUp(final SignUpView view, User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("name", user.getName())
        .add("email", user.getEmail())
        .add("password", user.getPassword())
        .add("phone", user.getPhone())
        .add("id_user_level", String.valueOf(user.getLevel().getId()))
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_SIGN_UP)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(new User(), jsonObject.getString("status"));
                } else {
                  view.onFailed("Daftar gagal");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Daftar gagal");
              }
            }
          }
        });
      }
    });
  }

  //untuk melakukan pendaftaran user baru
  public void update(final UpdateView view, User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("id", String.valueOf(user.getId()))
        .add("name", user.getName())
        .add("email", user.getEmail())
        .add("password", user.getPassword())
        .add("phone", user.getPhone())
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_UPDATE)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(new User(), jsonObject.getString("status"));
                } else {
                  view.onFailed("Update gagal");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Update gagal");
              }
            }
          }
        });
      }
    });
  }

  //untuk menampilkan identitas user pada aplikasi ini
  public void show(final ShowView view, User user) {
    //request ini untuk menjalankan servis http ke server
    request =
        new Request.Builder().url(Api.BASE_URL + Api.USER_GET + "?id=" + user.getId()).get()
            //http request post
            .build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  User result = new User();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  JSONObject dataObject = jsonArray.getJSONObject(0);
                  result.setId(dataObject.getInt("id"));
                  result.setName(dataObject.getString("name"));
                  result.setEmail(dataObject.getString("email"));
                  result.setPhone(dataObject.getString("phone"));
                  result.setPassword(dataObject.getString("password"));
                  view.onSuccess(result, jsonObject.getString("status"));
                } else {
                  view.onFailed("User tidak ditemukan");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("User tidak ditemukan");
              }
            }
          }
        });
      }
    });
  }

  //untuk menampilkan identitas user pada aplikasi ini
  public void getAll(final AllView view) {
    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_LIST).get()
        //http request post
        .build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  List<User> results = new ArrayList<>();
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  if (jsonArray != null && jsonObject.getInt("rows") > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                      JSONObject dataObject = jsonArray.getJSONObject(i);
                      User result = new User();
                      result.setId(dataObject.getInt("id"));
                      result.setName(dataObject.getString("name"));
                      result.setEmail(dataObject.getString("email"));
                      result.setPhone(dataObject.getString("phone"));
                      UserLevel level = new UserLevel(dataObject.getInt("id_user_level"));
                      level.setNama(dataObject.getString("name_level"));
                      result.setLevel(level);
                      results.add(result);
                    }
                  }

                  view.onSuccess(results,
                      jsonObject.getInt("rows") > 0 ? Message.LIST_RESULT_SUCCESS
                          : Message.LIST_RESULT_NOT_FOUND);
                } else {
                  view.onFailed(Message.LIST_RESULT_FAILED);
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed(Message.LIST_RESULT_FAILED);
              }
            }
          }
        });
      }
    });
  }

  //untuk menampilkan identitas user pada aplikasi ini
  public void forgotEmail(final ForgotView view, User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("email", user.getEmail()).build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_FORGOT)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  JSONArray jsonArray = jsonObject.getJSONArray("data");
                  JSONObject dataObject = jsonArray.getJSONObject(0);
                  User result = new User();
                  result.setId(dataObject.getInt("id"));
                  view.onSuccess(result, jsonObject.getString("status"));
                } else {
                  view.onFailed("List anggota tidak ditemukan");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("List anggota tidak ditemukan");
              }
            }
          }
        });
      }
    });
  }

  //untuk menampilkan identitas user pada aplikasi ini
  public void forgotPassword(final ForgotView view, User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("id", String.valueOf(user.getId()))
        .add("password", user.getPassword())
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_FORGOT)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onSuccess(new User(), jsonObject.getString("status"));
                } else {
                  view.onFailed("Password tidak berhasil di gantikan");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Password tidak berhasil di gantikan");
              }
            }
          }
        });
      }
    });
  }

  //untuk menampilkan identitas user pada aplikasi ini
  public void delete(final DeleteView view, User user) {
    //FormBody.Builder ini bertujuan untuk mengirim data dengan beberapa parameter ke server
    body = new FormBody.Builder().add("id", String.valueOf(user.getId()))
        .add("id_user_level", String.valueOf(user.getLevel().getId()))
        .build();

    //request ini untuk menjalankan servis http ke server
    request = new Request.Builder().url(Api.BASE_URL + Api.USER_DELETE)
        //http request post
        .post(body).build();

    //untuk melakukan asynchronous call, agar pengambilan data tidak dieksekusi di foreground android atau antar muka android
    client.newCall(request).enqueue(new Callback() {
      @Override public void onFailure(Call call, final IOException e) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            view.onFailed(e.getMessage());
          }
        });
      }

      @Override public void onResponse(Call call, final Response response) throws IOException {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
          @Override public void run() {
            if (response.isSuccessful()) {
              try {
                JSONObject jsonObject = new JSONObject(response.body().string());
                if (jsonObject.getString("status").equals("success")) {
                  view.onDeleteSuccess();
                } else {
                  view.onFailed("Terjadi kesalahan saat menghapus");
                }
              } catch (JSONException | IOException e) {
                e.printStackTrace();
                view.onFailed("Terjadi kesalahan saat menghapus");
              }
            }
          }
        });
      }
    });
  }

  public interface LoginView extends BaseView<User> {
  }

  public interface SignUpView extends BaseView<User> {
  }

  public interface UpdateView extends BaseView<User> {
  }

  public interface ShowView extends BaseView<User> {
  }

  public interface AllView extends BaseView<List<User>> {
  }

  public interface ForgotView extends BaseView<User> {
  }

  public interface DeleteView {
    void onDeleteSuccess();

    void onFailed(String message);
  }
}
