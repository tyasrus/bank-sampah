package com.dicky.banksampah.app.service.config;
/**
 * Kelas ini digunakan sebagai penyimpan konstan url yang digunakan untuk berhubungan dengan server
 */
public interface Api {
    //baris ini -> statement untuk menyimpan url sebagai url dasar dari semua service
    String BASE_URL = "https://app-1506163803.000webhostapp.com/api/";
    //String BASE_URL = "http://10.10.2.218/sampah/api/";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk login pengguna
    String USER_LOGIN = "user/login.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menyimpan data pengguna
    String USER_SIGN_UP = "user/save.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk kondisi jika pengguna lupa password
    String USER_FORGOT = "user/forgot.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil data pengguna
    String USER_GET = "user/get.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk memperbarui data pengguna
    String USER_UPDATE = "user/update.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data pengguna
    String USER_LIST = "user/list.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menghapus data pengguna
    String USER_DELETE = "user/delete.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data pengguna
    String USER_LEVEL_ALL = "user/level/list.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menyimpan data tempat
    String TEMPAT_SAVE = "tempat/save.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil data tempat
    String TEMPAT_GET = "tempat/get.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data tempat
    String TEMPAT_ALL = "tempat/list.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk memperbarui data tempat
    String TEMPAT_UPDATE = "tempat/update.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menghapus data tempat
    String TEMPAT_DELETE = "tempat/delete.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengecek data tempat
    String TEMPAT_CHECKING = "tempat/check.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menyimpan data transaksi
    String TRANSAKSI_SAVE = "transaksi/save.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil semua data transaksi
    String TRANSAKSI_ALL = "transaksi/list.php";

    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk mengambil data saldo
    String SALDO_GET = "saldo/get.php";
    //baris ini -> statement untuk menyimpan url dari api yang diperlukan untuk menghapus data saldo
    String SALDO_DELETE = "saldo/delete.php";
}
