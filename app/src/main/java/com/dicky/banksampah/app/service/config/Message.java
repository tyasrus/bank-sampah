package com.dicky.banksampah.app.service.config;
/**
 * kelas ini berisi konstan string yang digunakan untuk default pesan saat mengambil data dari server
 *
 * konstan adalah sebuah pernyataan yang tidak boleh diubah lagi
 */
public interface Message {
  //baris ini -> statement untuk menyimpan pesan jika pengambilan semua data berhasil
  String LIST_RESULT_SUCCESS = "Data sukses diambil";
  //baris ini -> statement untuk menyimpan pesan jika pengambilan semua data gagal
  String LIST_RESULT_FAILED = "Data gagal diambil";
  //baris ini -> statement untuk menyimpan pesan jika pengambilan semua data tidak ditemukan
  String LIST_RESULT_NOT_FOUND = "Data tidak ditemukan";
}
