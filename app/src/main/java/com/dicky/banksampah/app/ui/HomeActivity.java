package com.dicky.banksampah.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.RelativeLayout;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.ui.beranda.AdminFragment;
import com.dicky.banksampah.app.ui.beranda.PenggunaFragment;
import com.dicky.banksampah.app.ui.tempat.TempatFragment;
import com.dicky.banksampah.app.ui.transaksi.TransaksiFragment;
import com.dicky.banksampah.app.ui.user.LoginActivity;
import com.dicky.banksampah.app.ui.user.ProfileActivity;
import com.dicky.banksampah.app.util.UserUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.nav_view) NavigationView navView;
  @BindView(R.id.drawer_layout) DrawerLayout drawerLayout;
  @BindView(R.id.container) RelativeLayout container;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_home);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);

    ActionBarDrawerToggle toggle =
        new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open,
            R.string.navigation_drawer_close);
    drawerLayout.addDrawerListener(toggle);
    toggle.syncState();
    if (UserUtil.getInstance(this).getUser().getLevel().getId() == 2) {
      navView.getMenu().findItem(R.id.nav_anggota).setVisible(false);
      navView.setCheckedItem(R.id.nav_tempat);
      getSupportFragmentManager().beginTransaction()
          .add(R.id.container, new TempatFragment())
          .commit();
    } else if (UserUtil.getInstance(this).getUser().getLevel().getId() == 3) {
      navView.getMenu().findItem(R.id.nav_anggota).setVisible(false);
      navView.getMenu().findItem(R.id.nav_tempat).setVisible(false);
      getSupportFragmentManager().beginTransaction()
          .add(R.id.container, new PenggunaFragment())
          .commit();
    } else {
      navView.getMenu().findItem(R.id.nav_transaksi).setVisible(false);
      navView.getMenu().findItem(R.id.nav_tempat).setVisible(false);
      navView.getMenu().findItem(R.id.nav_anggota).setChecked(true);
      getSupportFragmentManager().beginTransaction()
          .add(R.id.container, new AdminFragment())
          .commit();
    }

    navView.setNavigationItemSelectedListener(this);
  }

  @Override public void onBackPressed() {
    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
      drawerLayout.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  @SuppressWarnings("StatementWithEmptyBody") @Override
  public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_anggota) {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.container, new AdminFragment())
          .addToBackStack("")
          .commit();
    } else if (id == R.id.nav_transaksi) {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.container, new TransaksiFragment())
          .addToBackStack("")
          .commit();
    } else if (id == R.id.nav_tempat) {
      getSupportFragmentManager().beginTransaction()
          .replace(R.id.container, new TempatFragment())
          .addToBackStack("")
          .commit();
    } else if (id == R.id.nav_profile) {
      startActivity(new Intent(this, ProfileActivity.class));
    } else if (id == R.id.nav_logout) {
      UserUtil.getInstance(this).logout();
      startActivity(new Intent(this, LoginActivity.class));
      finish();
    }

    drawerLayout.closeDrawer(GravityCompat.START);
    return true;
  }
}
