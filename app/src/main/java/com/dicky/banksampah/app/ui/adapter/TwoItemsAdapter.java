package com.dicky.banksampah.app.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.entity.Transaksi;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.ui.adapter.holder.ThreeItemHolder;
import com.dicky.banksampah.app.ui.adapter.holder.TwoItemHolder;
import com.dicky.banksampah.app.util.DateUtil;

import java.util.Calendar;
import java.util.List;

/**
 * Created by tyas on 9/30/17.
 */

public class TwoItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements TwoItemHolder.OnTwoItemClickListener, ThreeItemHolder.OnThreeItemClickListener {

    private List<Tempat> tempats;
    private List<Transaksi> transaksis;
    private List<User> users;
    private OnItemClickListener onItemClickListener;

    public void setTempats(List<Tempat> tempats) {
        this.tempats = tempats;
    }

    public void setTransaksis(List<Transaksi> transaksis) {
        this.transaksis = transaksis;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (tempats != null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_two_list, parent, false);
            TwoItemHolder itemHolder = new TwoItemHolder(view);
            itemHolder.setOnTwoItemClickListener(this);
            return itemHolder;
        }

        if (transaksis != null || users != null) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_three_list, parent, false);
            ThreeItemHolder itemHolder = new ThreeItemHolder(view);
            itemHolder.setOnThreeItemClickListener(this);
            return itemHolder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof TwoItemHolder && tempats != null) {
            TwoItemHolder itemHolder = (TwoItemHolder) holder;
            Tempat tempat = tempats.get(position);
            itemHolder.tvTitle.setText(tempat.getNama());
            itemHolder.tvSubtitle.setText(tempat.getAlamat());
        }

        if (holder instanceof ThreeItemHolder && transaksis != null) {
            ThreeItemHolder itemHolder = (ThreeItemHolder) holder;
            Transaksi transaksi = transaksis.get(position);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(transaksi.getWaktu());
            itemHolder.tvTitle.setText(DateUtil.getIndonesianDay(calendar.get(Calendar.DAY_OF_WEEK))
                    + ", "
                    + calendar.get(Calendar.DAY_OF_MONTH)
                    + " "
                    + DateUtil.getIndonesianMonth(calendar.get(Calendar.MONTH))
                    + ", "
                    + DateUtil.get24Hours(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE)));
//            itemHolder.tvTitle.setText(transaksi.getWaktu().toString());
            itemHolder.tvSubtitle.setText("Total Sampah : " + transaksi.getTotalSampah() + " Kg");
            itemHolder.tvCompliment.setText("Rp. " + transaksi.getTotalHarga());
        }

        if (holder instanceof ThreeItemHolder && users != null) {
            ThreeItemHolder itemHolder = (ThreeItemHolder) holder;
            User user = users.get(position);

            itemHolder.tvTitle.setText(user.getName());
//            itemHolder.tvTitle.setText(transaksi.getWaktu().toString());
            itemHolder.tvSubtitle.setText(user.getPhone());
            itemHolder.tvCompliment.setText(user.getLevel().getNama());
        }
    }

    @Override
    public int getItemCount() {
        return tempats != null ? tempats.size() : transaksis != null ? transaksis.size() : users.size();
    }

    @Override
    public void onTwoItemClick(View view, int position) {
        if (onItemClickListener != null) onItemClickListener.onItemClick(view, position);
    }

    @Override
    public void onTwoLongItemClick(View view, int position) {
        if (onItemClickListener != null) onItemClickListener.onLongItemClick(view, position);
    }

    @Override
    public void onThreeItemClick(View view, int position) {
        if (onItemClickListener != null) onItemClickListener.onItemClick(view, position);
    }

    @Override
    public void onThreeLongItemClick(View view, int position) {
        if (onItemClickListener != null) onItemClickListener.onLongItemClick(view, position);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onLongItemClick(View view, int position);
    }
}
