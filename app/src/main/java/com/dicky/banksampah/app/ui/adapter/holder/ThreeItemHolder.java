package com.dicky.banksampah.app.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.dicky.banksampah.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tyas on 10/1/17.
 */

public class ThreeItemHolder extends RecyclerView.ViewHolder
    implements View.OnLongClickListener, View.OnClickListener {

  @BindView(R.id.tv_title) public TextView tvTitle;
  @BindView(R.id.tv_subtitle) public TextView tvSubtitle;
  @BindView(R.id.tv_compliment) public TextView tvCompliment;

  private OnThreeItemClickListener onThreeItemClickListener;

  public ThreeItemHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
    itemView.setOnClickListener(this);
    itemView.setOnLongClickListener(this);
  }

  public void setOnThreeItemClickListener(OnThreeItemClickListener onThreeItemClickListener) {
    this.onThreeItemClickListener = onThreeItemClickListener;
  }

  @Override public boolean onLongClick(View v) {
    if (onThreeItemClickListener != null) {
      onThreeItemClickListener.onThreeLongItemClick(v, getAdapterPosition());
    }
    return false;
  }

  @Override public void onClick(View v) {
    if (onThreeItemClickListener != null) {
      onThreeItemClickListener.onThreeItemClick(v, getAdapterPosition());
    }
  }

  public interface OnThreeItemClickListener {
    void onThreeItemClick(View view, int position);

    void onThreeLongItemClick(View view, int position);
  }
}
