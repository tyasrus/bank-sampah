package com.dicky.banksampah.app.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.dicky.banksampah.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tyas on 9/30/17.
 */

public class TwoItemHolder extends RecyclerView.ViewHolder
    implements View.OnClickListener, View.OnLongClickListener {

  @BindView(R.id.tv_title) public TextView tvTitle;
  @BindView(R.id.tv_subtitle) public TextView tvSubtitle;

  private OnTwoItemClickListener onTwoItemClickListener;

  public TwoItemHolder(View itemView) {
    super(itemView);
    ButterKnife.bind(this, itemView);
    itemView.setOnClickListener(this);
    itemView.setOnLongClickListener(this);
  }

  public void setOnTwoItemClickListener(OnTwoItemClickListener onTwoItemClickListener) {
    this.onTwoItemClickListener = onTwoItemClickListener;
  }

  @Override public void onClick(View v) {
    if (onTwoItemClickListener != null) {
      onTwoItemClickListener.onTwoItemClick(v, getAdapterPosition());
    }
  }

  @Override public boolean onLongClick(View v) {
    if (onTwoItemClickListener != null) {
      onTwoItemClickListener.onTwoLongItemClick(v, getAdapterPosition());
    }
    return false;
  }

  public interface OnTwoItemClickListener {
    void onTwoItemClick(View view, int position);

    void onTwoLongItemClick(View view, int position);
  }
}
