package com.dicky.banksampah.app.ui.beranda;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.UserService;
import com.dicky.banksampah.app.ui.adapter.TwoItemsAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdminFragment extends Fragment
    implements TwoItemsAdapter.OnItemClickListener, UserService.AllView, UserService.DeleteView {

  Unbinder unbinder;
  @BindView(R.id.recycler) RecyclerView recycler;

  private List<User> users;
  private TwoItemsAdapter adapter;

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_admin, container, false);
    unbinder = ButterKnife.bind(this, view);
    users = new ArrayList<>();
    adapter = new TwoItemsAdapter();
    adapter.setUsers(users);
    adapter.setOnItemClickListener(this);
    recycler.setLayoutManager(
        new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    recycler.setAdapter(adapter);
    UserService.getInstance().getAll(this);
    return view;
  }

  @Override public void onResume() {
    super.onResume();
    getActivity().setTitle("Beranda");
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override public void onItemClick(View view, int position) {

  }

  @Override public void onLongItemClick(View view, final int position) {
    new AlertDialog.Builder(getActivity()).setTitle("Hapus User")
        .setMessage("Hapus " + users.get(position).getName() + "?")
        .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
          }
        })
        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
          @Override public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            UserService.getInstance().delete(AdminFragment.this, users.get(position));
          }
        })
        .show();
  }

  @Override public void onSuccess(List<User> users, String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    if (!this.users.isEmpty()) {
      this.users.clear();
      adapter.notifyDataSetChanged();
    }

    this.users.addAll(users);
    adapter.notifyDataSetChanged();
  }

  @Override public void onDeleteSuccess() {
    Toast.makeText(getActivity(), "Berhasil menghapus user", Toast.LENGTH_SHORT).show();
    UserService.getInstance().getAll(this);
  }

  @Override public void onFailed(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }
}
