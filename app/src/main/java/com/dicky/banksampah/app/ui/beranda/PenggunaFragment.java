package com.dicky.banksampah.app.ui.beranda;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Saldo;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.SaldoService;
import com.dicky.banksampah.app.service.TempatService;
import com.dicky.banksampah.app.service.config.BaseView;
import com.dicky.banksampah.app.ui.tempat.FormTempatActivity;
import com.dicky.banksampah.app.util.UserUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PenggunaFragment extends Fragment
    implements TempatService.GetView, BaseView<Saldo>, TempatService.CheckingView,
    SaldoService.DeleteView {

  @BindView(R.id.tv_saldo_title) TextView tvSaldoTitle;
  @BindView(R.id.tv_saldo_value) TextView tvSaldoValue;
  @BindView(R.id.tv_total_sampah_title) TextView tvTotalSampahTitle;
  @BindView(R.id.tv_total_sampah_value) TextView tvTotalSampahValue;
  @BindView(R.id.tv_tempat) TextView tvTempat;
  @BindView(R.id.img_tempat) ImageView imgTempat;
  @BindView(R.id.tempat_reminder) LinearLayout tempatReminder;
  @BindView(R.id.tv_status_jemput_title) TextView tvStatusJemputTitle;
  @BindView(R.id.tv_status_jemput_value) TextView tvStatusJemputValue;
  private Unbinder unbinder;

  private boolean isTempatAvailable = false;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_pengguna, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @Override public void onResume() {
    super.onResume();
    getActivity().setTitle("Beranda");
    refresh();
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_refresh, menu);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_refresh) {
      new AlertDialog.Builder(getActivity()).setTitle("Segarkan Data")
          .setMessage("Anda akan menyegarkan data dari halaman beranda ini, lakukan?")
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          })
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
              refresh();
            }
          })
          .show();
    }

    return super.onOptionsItemSelected(item);
  }

  @OnClick({ R.id.tempat_reminder, R.id.cash_out_container })
  public void onRemincerClickListener(View view) {
    if (view.getId() == R.id.tempat_reminder) {
      if (!isTempatAvailable) {
        startActivity(new Intent(getActivity(), FormTempatActivity.class));
      }
    }

    if (view.getId() == R.id.cash_out_container) {
      new AlertDialog.Builder(getActivity()).setTitle("Segarkan Data")
          .setMessage("Anda akan cash out dan saldo anda akan kosong, lakukan?")
          .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          })
          .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
              SaldoService.getInstance().delete(PenggunaFragment.this, new User(UserUtil.getInstance(getContext()).getId()));
            }
          })
          .show();

    }
  }

  private void refresh() {
    Tempat tempat = new Tempat();
    tempat.setUser(new User(UserUtil.getInstance(getContext()).getId()));
    TempatService.getInstance().get(this, tempat);
    TempatService.getInstance().checking(this, tempat);

    Saldo saldo = new Saldo();
    saldo.setUser(new User(UserUtil.getInstance(getContext()).getId()));
    SaldoService.getInstance().get(this, saldo);
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override public void onSuccess(Tempat tempat, String message) {
    isTempatAvailable = true;
    Toast.makeText(getActivity(), "Anda sudah menambahkan alamat", Toast.LENGTH_SHORT).show();
    tempatReminder.setBackgroundResource(R.drawable.bg_stroke_top_bottom);
    tvTempat.setText(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? Html.fromHtml(
        "<b>Alamat Anda : </b><br/><b>Nama : </b> "
            + tempat.getNama()
            + "<br />"
            + "<b>Alamat : </b>"
            + tempat.getAlamat()
            + "<br />"
            + "<b>Koordinat : </b>"
            + tempat.getLatitude()
            + ", "
            + tempat.getLongitude()
            + "<br />", Html.FROM_HTML_MODE_COMPACT) : Html.fromHtml(
        "<b>Alamat Anda : </b><br/><b>Nama : </b> "
            + tempat.getNama()
            + "<br />"
            + "<b>Alamat : </b>"
            + tempat.getAlamat()
            + "<br />"
            + "<b>Koordinat : </b>"
            + tempat.getLatitude()
            + ", "
            + tempat.getLongitude()));
    tvTempat.setTextColor(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
        R.color.textSecondary, null) : getResources().getColor(R.color.textSecondary));
    imgTempat.setImageResource(R.drawable.ic_done_gray_24dp);
  }

  @Override public void onSuccess(Saldo saldo, String message) {
    if (tvSaldoValue != null) tvSaldoValue.setText(String.valueOf(saldo.getTotalBayar()));

    if (tvTotalSampahValue != null) {
      tvTotalSampahValue.setText(String.valueOf(saldo.getTotalSampah()) + " Kg");
    }
  }

  @Override public void onSuccess(Boolean aBoolean, String message) {
    tvStatusJemputValue.setText(aBoolean ? "Sudah Dijemput" : "Belum Dijemput");
    tvStatusJemputValue.setTextColor(
        aBoolean ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.green, null) : getResources().getColor(R.color.green)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(R.color.red,
                null) : getResources().getColor(R.color.red));
  }

  @Override public void onFailed(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onSuccess() {
    Toast.makeText(getActivity(), "Cash Out Berhasil!", Toast.LENGTH_SHORT).show();
    refresh();
  }

  @Override public void onError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }
}
