package com.dicky.banksampah.app.ui.tempat;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.config.BaseView;
import com.dicky.banksampah.app.service.TempatService;
import com.dicky.banksampah.app.util.LocationUtil;
import com.dicky.banksampah.app.util.TempatUtil;
import com.dicky.banksampah.app.util.UserUtil;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormTempatActivity extends AppCompatActivity
    implements LocationUtil.TrackingLocation, TempatService.GetView, BaseView<Tempat> {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.name) EditText name;
  @BindView(R.id.ti_name) TextInputLayout tiName;
  @BindView(R.id.alamat) EditText alamat;
  @BindView(R.id.ti_alamat) TextInputLayout tiAlamat;
  @BindView(R.id.latitude) EditText latitude;
  @BindView(R.id.ti_lat) TextInputLayout tiLat;
  @BindView(R.id.longitude) EditText longitude;
  @BindView(R.id.ti_lng) TextInputLayout tiLng;
  @BindView(R.id.btn_location) Button btnLocation;
  @BindView(R.id.btn_kirim) Button btnKirim;

  private LocationUtil locationUtil;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_form);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    if (getIntent().getIntExtra("id", 0) > 0) {
      setTitle("Edit Tempat");
      TempatService.getInstance().get(this, new Tempat(getIntent().getIntExtra("id", 0)));
    } else {
      setTitle("Tambahkan Tempat");
    }

    locationUtil = new LocationUtil(this);
    locationUtil.setTrackingLocation(this);
    locationUtil.connect();
  }

  @Override protected void onStart() {
    super.onStart();
    if (TempatUtil.getInstance(this).getLatLng() != null) {
      send(false, btnLocation, "Cari Lokasi");
      latitude.setText(String.valueOf(TempatUtil.getInstance(this).getLatLng().latitude));
      longitude.setText(String.valueOf(TempatUtil.getInstance(this).getLatLng().longitude));
      TempatUtil.getInstance(this).setLatLng(0, 0);
    }

    if (TempatUtil.getInstance(this).getName() != null) {
      alamat.setText(TempatUtil.getInstance(this).getName());
      TempatUtil.getInstance(this).setName("");
    }

    if (locationUtil.getSearchLocationStatus()) locationUtil.find();
  }

  @OnClick({ R.id.btn_kirim, R.id.btn_location }) public void onFormButtonClickListener(View view) {
    if (view.getId() == R.id.btn_kirim) {
      if (name.getText().toString().trim().length() == 0) {
        name.setError("Nama harus di isi dulu");
      } else if (alamat.getText().toString().trim().length() == 0) {
        alamat.setError("Alamat harus di isi dulu");
      } else if (latitude.getText().toString().trim().length() == 0) {
        latitude.setError("Latitude masih kosong, silahkan cari lokasi");
      } else if (longitude.getText().toString().trim().length() == 0) {
        longitude.setError("Latitude masih kosong, silahkan cari lokasi");
      } else {
        Tempat tempat = new Tempat();
        tempat.setNama(name.getText().toString());
        tempat.setAlamat(alamat.getText().toString());
        tempat.setLatitude(Double.parseDouble(latitude.getText().toString()));
        tempat.setLongitude(Double.parseDouble(longitude.getText().toString()));
        tempat.setUser(new User(UserUtil.getInstance(this).getId()));
        if (getIntent().getIntExtra("id", 0) > 0) {
          tempat.setId(getIntent().getIntExtra("id", 0));
          TempatService.getInstance().update(this, tempat);
        } else {
          TempatService.getInstance().save(this, tempat);
        }
      }
    }

    if (view.getId() == R.id.btn_location) {
      if (name.getText().toString().trim().length() == 0) {
        name.setError("Nama harus di isi dulu");
      } else if (alamat.getText().toString().trim().length() == 0) {
        alamat.setError("Alamat harus di isi dulu");
      } else {
        new AlertDialog.Builder(this).setTitle("Tentukan Lokasi")
            .setItems(new String[] { "Lokasi Saya", "Cari Lokasi Lain" },
                new DialogInterface.OnClickListener() {
                  @Override public void onClick(DialogInterface dialog, int which) {
                    if (which == 0) {
                      locationUtil.find();
                    }

                    if (which == 1) {
                      startActivity(
                          new Intent(FormTempatActivity.this, MapsActivity.class).putExtra("alamat",
                              alamat.getText().toString()));
                    }
                  }
                })
            .show();
        send(true, btnLocation, "Tunggu Sebentar ...");
      }
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) finish();

    return super.onOptionsItemSelected(item);
  }

  private void send(boolean isSending, Button button, String buttonName) {
    button.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorAccent);
    button.setText(buttonName);
    button.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorAccent, null) : getResources().getColor(R.color.colorAccent)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }

  @Override public void onSuccess(Tempat tempat, String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    if (tempat != null) {
      name.setText(tempat.getNama());
      alamat.setText(tempat.getAlamat());
      latitude.setText(String.valueOf(tempat.getLatitude()));
      longitude.setText(String.valueOf(tempat.getLongitude()));
    } else {
      finish();
    }
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onLocationFound(Location location) {
    locationUtil.setSearchLocationStatus(false);
    send(false, btnLocation, "Cari Lokasi");
    latitude.setText(String.valueOf(location.getLatitude()));
    longitude.setText(String.valueOf(location.getLongitude()));
    //        System.out.println("testing location : " + placeName(new LatLng(location.getLatitude(), location.getLongitude())));
    alamat.setText(placeName(new LatLng(location.getLatitude(), location.getLongitude())));
  }

  @Override public void onLocationError(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override public void isGpsEnabled(boolean isEnabled) {
    if (!isEnabled) startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
  }

  private String placeName(LatLng latLng) {
    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
    List<Address> addresses = null;
    try {
      addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
    } catch (IOException e) {
      e.printStackTrace();
    }

    System.out.println("array place name : " + addresses.toString());
    //        String cityName = addresses.get(0).getAddressLine(0);
    //        String stateName = addresses != null ? addresses.get(0).getAddressLine(1) : "Place not Defined";
    //        String countryName = addresses.get(0).getAddressLine(2);
    return addresses.get(0).getAddressLine(0);
  }
}
