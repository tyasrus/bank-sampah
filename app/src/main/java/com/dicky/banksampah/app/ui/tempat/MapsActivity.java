package com.dicky.banksampah.app.ui.tempat;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.util.TempatUtil;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapsActivity extends FragmentActivity
    implements OnMapReadyCallback, PlaceSelectionListener, GoogleMap.OnMapClickListener {

  @BindView(R.id.autocomplete_container) RelativeLayout autocompleteContainer;
  @BindView(R.id.btn_direction) Button btnDirection;

  private GoogleMap mMap;
  private boolean isLoaded = false;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);
    ButterKnife.bind(this);
    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment =
        (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    PlaceAutocompleteFragment autocompleteFragment =
        (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete);
    autocompleteFragment.setOnPlaceSelectedListener(this);
    autocompleteFragment.setText(getIntent().getStringExtra("alamat"));
    btnDirection.setVisibility(View.GONE);
  }

  @OnClick(R.id.btn_direction) public void onDirectionClick(View view) {
    if (isLoaded) {
      Tempat tempat = (Tempat) getIntent().getParcelableExtra("tempat");
      String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)",
          tempat.getLatitude(), tempat.getLongitude(), tempat.getNama());
      Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
      intent.setPackage("com.google.android.apps.maps");
      startActivity(intent);
    }
  }

  /**
   * Manipulates the map once available.
   * This callback is triggered when the map is ready to be used.
   * This is where we can add markers or lines, add listeners or move the camera. In this case,
   * we just add a marker near Sydney, Australia.
   * If Google Play services is not installed on the device, the user will be prompted to install
   * it inside the SupportMapFragment. This method will only be triggered once the user has
   * installed Google Play services and returned to the app.
   */
  @Override public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    mMap.setOnMapClickListener(this);

    // Add a marker in Sydney and move the camera
    if (getIntent().getParcelableExtra("tempat") != null) {
      isLoaded = true;
      Tempat tempat = (Tempat) getIntent().getParcelableExtra("tempat");
      mMap.addMarker(
          new MarkerOptions().position(new LatLng(tempat.getLatitude(), tempat.getLongitude()))
              .title(tempat.getNama() + ", " + tempat.getAlamat()));
      mMap.moveCamera(
          CameraUpdateFactory.newLatLngZoom(new LatLng(tempat.getLatitude(), tempat.getLongitude()),
              17));
      autocompleteContainer.setVisibility(View.GONE);
      btnDirection.setVisibility(View.VISIBLE);
    } else {
      LatLng bekasi = new LatLng(-6.2845348, 106.9033364);
      mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bekasi, 17));
    }
  }

  @Override public void onPlaceSelected(Place place) {
    LatLng selectedPlace = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
    TempatUtil.getInstance(this).setName(place.getName().toString());
    TempatUtil.getInstance(this).setLatLng(place.getLatLng().latitude, place.getLatLng().longitude);
    mMap.addMarker(
        new MarkerOptions().position(selectedPlace).title(getIntent().getStringExtra("alamat")));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedPlace, 17));
    finish();
  }

  @Override public void onError(Status status) {
    Toast.makeText(this, status.getStatusMessage(), Toast.LENGTH_SHORT).show();
  }

  @Override public void onMapClick(LatLng latLng) {
    mMap.clear();
    mMap.addMarker(
        new MarkerOptions().position(latLng).title(getIntent().getStringExtra("alamat")));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));
    TempatUtil.getInstance(this).setName(placeName(latLng));
    TempatUtil.getInstance(this).setLatLng(latLng.latitude, latLng.longitude);
    finish();
  }

  private String placeName(LatLng latLng) {
    Geocoder geocoder = new Geocoder(this, Locale.getDefault());
    List<Address> addresses = null;
    try {
      addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
    } catch (IOException e) {
      e.printStackTrace();
    }
    //        String cityName = addresses.get(0).getAddressLine(0);
    String stateName = addresses != null ? addresses.get(0).getAddressLine(1) : "Place not Defined";
    //        String countryName = addresses.get(0).getAddressLine(2);
    return addresses.get(0).getAddressLine(1);
  }
}
