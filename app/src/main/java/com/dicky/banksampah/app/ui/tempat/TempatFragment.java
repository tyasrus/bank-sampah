package com.dicky.banksampah.app.ui.tempat;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.service.TempatService;
import com.dicky.banksampah.app.ui.adapter.TwoItemsAdapter;
import com.dicky.banksampah.app.ui.transaksi.FormTransaksiActivity;
import com.dicky.banksampah.app.util.LocationUtil;
import com.dicky.banksampah.app.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TempatFragment extends Fragment
    implements TempatService.AllView, TwoItemsAdapter.OnItemClickListener, TempatService.DeleteView,
    LocationUtil.TrackingLocation {

  @BindView(R.id.recycler) RecyclerView recycler;
  Unbinder unbinder;

  private List<Tempat> tempats;
  private TwoItemsAdapter adapter;
  private LocationUtil locationUtil;

  @Override public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_tempat, container, false);
    unbinder = ButterKnife.bind(this, view);
    locationUtil = new LocationUtil(getActivity());
    locationUtil.setTrackingLocation(this);
    locationUtil.connect();
    tempats = new ArrayList<>();
    adapter = new TwoItemsAdapter();
    adapter.setTempats(tempats);
    adapter.setOnItemClickListener(this);
    recycler.setLayoutManager(
        new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    recycler.setAdapter(adapter);
    return view;
  }

  @Override public void onStart() {
    super.onStart();
    Tempat tempat = new Tempat();
    tempat.setUser(UserUtil.getInstance(getContext()).getUser());
    TempatService.getInstance().getAll(this, tempat);
  }

  @Override public void onResume() {
    super.onResume();
    getActivity().setTitle("Daftar Tempat");
    if (locationUtil.getSearchLocationStatus()) locationUtil.find();
  }

  @Override public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.menu_add, menu);
    if (UserUtil.getInstance(getContext()).getUser().getLevel().getId() != 1) {
      menu.findItem(R.id.action_add).setVisible(false);
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_add) {
      startActivity(new Intent(getActivity(), FormTempatActivity.class));
    }

    if (item.getItemId() == R.id.action_nearby) {
      locationUtil.find();
      locationUtil.setSearchLocationStatus(true);
    }

    return super.onOptionsItemSelected(item);
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override public void onSuccess(List<Tempat> tempats, String message) {
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    if (!this.tempats.isEmpty()) {
      this.tempats.clear();
      adapter.notifyDataSetChanged();
    }

    this.tempats.addAll(tempats);
    adapter.notifyDataSetChanged();
  }

  @Override public void onSuccess(String message) {
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    Tempat tempat = new Tempat();
    tempat.setUser(UserUtil.getInstance(getContext()).getUser());
    TempatService.getInstance().getAll(this, tempat);
  }

  @Override public void onFailed(String message) {
    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onItemClick(View view, int position) {
    if (UserUtil.getInstance(getContext()).getUser().getLevel().getId() == 1) {
      startActivity(new Intent(getActivity(), FormTempatActivity.class).putExtra("id",
          tempats.get(position).getId()));
    } else {
      final Tempat tempat = tempats.get(position);
      String message = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? Html.fromHtml(
          "<b>Nama : </b> "
              + tempat.getNama()
              + "<br />"
              + "<b>Alamat : </b>"
              + tempat.getAlamat()
              + "<br />"
              + "<b>Koordinat : </b>"
              + tempat.getLatitude()
              + ", "
              + tempat.getLongitude()
              + "<br />", Html.FROM_HTML_MODE_COMPACT).toString() : Html.fromHtml("<b>Nama : </b> "
          + tempat.getNama()
          + "<br />"
          + "<b>Alamat : </b>"
          + tempat.getAlamat()
          + "<br />"
          + "<b>Koordinat : </b>"
          + tempat.getLatitude()
          + ", "
          + tempat.getLongitude()
          + "<br />").toString();
      new AlertDialog.Builder(getActivity()).setTitle("Detail Tempat")
          .setItems(new String[] { message, "Buat Transaksi", "Lihat di Peta", "Selesai" },
              new DialogInterface.OnClickListener() {
                @Override public void onClick(DialogInterface dialog, int which) {
                  switch (which) {
                    case 1:
                      startActivity(
                          new Intent(getActivity(), FormTransaksiActivity.class).putExtra("tempat",
                              tempat));
                      break;
                    case 2:
                      startActivity(
                          new Intent(getActivity(), MapsActivity.class).putExtra("tempat", tempat));
                      break;
                    case 3:
                      break;
                    default:
                      dialog.dismiss();
                  }
                }
              })
          .show();
    }
  }

  @Override public void onLongItemClick(View view, final int position) {
    if (UserUtil.getInstance(getContext()).getUser().getLevel().getId() == 1) {
      new AlertDialog.Builder(getActivity()).setTitle("Hapus Item")
          .setMessage("Hapus " + tempats.get(position).getNama() + "?")
          .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              TempatService.getInstance().delete(TempatFragment.this, tempats.get(position));
              dialog.dismiss();
            }
          })
          .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          })
          .show();
    }
  }

  @Override public void onLocationFound(Location location) {
    locationUtil.setSearchLocationStatus(false);
    Tempat tempat = new Tempat();
    tempat.setLatitude(location.getLatitude());
    tempat.setLongitude(location.getLongitude());
    TempatService.getInstance().getNearbys(this, tempat);
  }

  @Override public void onLocationError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    locationUtil.setSearchLocationStatus(false);
  }

  @Override public void isGpsEnabled(boolean isEnabled) {
    if (!isEnabled) {
      new AlertDialog.Builder(getActivity()).setTitle("Lokasi Tidak Aktif")
          .setMessage("Layanan GPS belum aktif, aftifkan?")
          .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
              startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
          })
          .setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
              dialog.dismiss();
            }
          })
          .show();
    }
  }
}
