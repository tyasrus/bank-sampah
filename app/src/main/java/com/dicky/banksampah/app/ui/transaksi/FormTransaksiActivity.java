package com.dicky.banksampah.app.ui.transaksi;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.entity.Transaksi;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.TransaksiService;
import com.dicky.banksampah.app.service.config.BaseView;
import com.dicky.banksampah.app.util.UserUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormTransaksiActivity extends AppCompatActivity implements BaseView<Transaksi> {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.tv_alamat) TextView tvAlamat;
  @BindView(R.id.total_harga) EditText totalHarga;
  @BindView(R.id.textInputLayout6) TextInputLayout textInputLayout6;
  @BindView(R.id.total_sampah) EditText totalSampah;
  @BindView(R.id.btn_kirim) Button btnKirim;
  @BindView(R.id.cb_status_bayar) CheckBox cbStatusBayar;

  private Tempat tempat;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_form2);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    tempat = (Tempat) getIntent().getParcelableExtra("tempat");
    tvAlamat.setText(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ? Html.fromHtml(
        "<b>Alamat : </b>" + tempat.getAlamat() + "<br />", Html.FROM_HTML_MODE_COMPACT)
        : Html.fromHtml("<b>Alamat : </b>" + tempat.getAlamat() + "<br />"));
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) finish();

    return super.onOptionsItemSelected(item);
  }

  @OnClick(R.id.btn_kirim) public void onTransaksiButtonClick(View view) {
    if (totalSampah.getText().toString().trim().length() == 0) {
      totalSampah.setError("Total Sampah masih kosong");
    } else if (totalHarga.getText().toString().trim().length() == 0) {
      totalHarga.setError("Total Harga masih kosong");
    } else {
      Transaksi transaksi = new Transaksi();
      transaksi.setTotalHarga(Integer.parseInt(totalHarga.getText().toString()));
      transaksi.setTotalSampah(Integer.parseInt(totalSampah.getText().toString()));
      transaksi.setUser(new User(UserUtil.getInstance(this).getId()));
      transaksi.setTempat(tempat);
      //transaksi.setStatusBayar(cbStatusBayar.isChecked());
      TransaksiService.getInstance().save(this, transaksi);
      send(true);
    }
  }

  private void send(boolean isSending) {
    totalHarga.setEnabled(!isSending);
    totalSampah.setEnabled(!isSending);
    btnKirim.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorAccent);
    btnKirim.setText(isSending ? "Tunggu Sebentar ..." : "Kirim");
    btnKirim.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorAccent, null) : getResources().getColor(R.color.colorAccent)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }

  @Override public void onSuccess(Transaksi transaksi, String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    finish();
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    send(false);
  }
}
