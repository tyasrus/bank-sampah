package com.dicky.banksampah.app.ui.transaksi;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Transaksi;
import com.dicky.banksampah.app.service.TransaksiService;
import com.dicky.banksampah.app.ui.adapter.TwoItemsAdapter;
import com.dicky.banksampah.app.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransaksiFragment extends Fragment
    implements TransaksiService.AllView, TwoItemsAdapter.OnItemClickListener {

  @BindView(R.id.recycler) RecyclerView recycler;
  Unbinder unbinder;

  private TwoItemsAdapter adapter;
  private List<Transaksi> transaksis;

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view = inflater.inflate(R.layout.fragment_transaksi, container, false);
    unbinder = ButterKnife.bind(this, view);
    int id = UserUtil.getInstance(getContext()).getUser().getId();
    int idUserLevel = UserUtil.getInstance(getContext()).getUser().getLevel().getId();
    TransaksiService.getInstance().getAll(this, id, idUserLevel);
    transaksis = new ArrayList<>();
    adapter = new TwoItemsAdapter();
    adapter.setTransaksis(transaksis);
    adapter.setOnItemClickListener(this);
    recycler.setLayoutManager(
        new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    recycler.setAdapter(adapter);
    return view;
  }

  @Override public void onResume() {
    super.onResume();
    getActivity().setTitle("Histori Transaksi");
  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
  }

  @Override public void onSuccess(List<Transaksi> transaksis, String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    this.transaksis.addAll(transaksis);
    adapter.notifyDataSetChanged();
  }

  @Override public void onFailed(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onItemClick(View view, int position) {

  }

  @Override public void onLongItemClick(View view, int position) {

  }
}
