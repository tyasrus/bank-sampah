package com.dicky.banksampah.app.ui.user;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.UserService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotActivity extends AppCompatActivity implements UserService.ForgotView {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.email) EditText email;
  @BindView(R.id.tI_email) TextInputLayout tIEmail;
  @BindView(R.id.password) EditText password;
  @BindView(R.id.tI_password) TextInputLayout tIPassword;
  @BindView(R.id.btn_forgot) Button btnForgot;

  private User forgottenUser;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_forgot);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    tIPassword.setVisibility(View.GONE);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    forgottenUser = new User();
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) finish();

    return super.onOptionsItemSelected(item);
  }

  @OnClick(R.id.btn_forgot) public void onForgotButtonClick(View view) {
    if (tIEmail.getVisibility() == View.VISIBLE
        && email.getText().toString().trim().length() == 0) {
      email.setError("Email masih kosong");
    } else if (tIEmail.getVisibility() == View.VISIBLE
        && email.getText().toString().trim().length() > 0) {
      User user = new User();
      user.setEmail(email.getText().toString());
      UserService.getInstance().forgotEmail(this, user);
      send(true);
    } else if (tIPassword.getVisibility() == View.VISIBLE
        && password.getText().toString().trim().length() == 0) {
      password.setError("Password masih kosong");
    } else if (tIPassword.getVisibility() == View.VISIBLE
        && password.getText().toString().trim().length() > 0) {
      forgottenUser.setPassword(password.getText().toString());
      UserService.getInstance().forgotPassword(this, forgottenUser);
      send(true);
    }
  }

  private void send(boolean isSending) {
    email.setEnabled(!isSending);
    password.setEnabled(!isSending);
    btnForgot.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorAccent);
    btnForgot.setText(isSending ? "Tunggu Sebentar ..." : "Kirim");
    btnForgot.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorAccent, null) : getResources().getColor(R.color.colorAccent)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }

  @Override public void onSuccess(User user, String message) {
    if (user.getId() == 0) {
      Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
      finish();
    } else {
      forgottenUser.setId(user.getId());
      Toast.makeText(this, "Silahkan masukkan password baru anda", Toast.LENGTH_LONG).show();
      tIEmail.setVisibility(View.GONE);
      tIPassword.setVisibility(View.VISIBLE);
      send(false);
    }
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    send(false);
  }
}
