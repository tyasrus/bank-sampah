package com.dicky.banksampah.app.ui.user;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.UserService;
import com.dicky.banksampah.app.ui.HomeActivity;
import com.dicky.banksampah.app.util.UserUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements UserService.LoginView {

  @BindView(R.id.img) ImageView img;
  @BindView(R.id.tv_title) TextView tvTitle;
  @BindView(R.id.email) EditText email;
  @BindView(R.id.password) EditText password;
  @BindView(R.id.textInputLayout) TextInputLayout textInputLayout;
  @BindView(R.id.btn_masuk) Button btnMasuk;
  @BindView(R.id.tv_forgot) TextView tvForgot;
  @BindView(R.id.tv_signup) TextView tvSignup;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_login);
    ButterKnife.bind(this);
    if (UserUtil.getInstance(this).isLogin()) {
      startActivity(new Intent(this, HomeActivity.class));
      finish();
    }
  }

  @OnClick({ R.id.btn_masuk, R.id.tv_forgot, R.id.tv_signup })
  public void onLoginButtonClick(View view) {
    if (view.getId() == R.id.btn_masuk) {
      if (email.getText().toString().trim().length() == 0) {
        email.setError("Email masih kosong");
      } else if (password.getText().toString().trim().length() == 0) {
        password.setError("Password masih kosong");
      } else {
        User user = new User();
        user.setEmail(email.getText().toString());
        user.setPassword(password.getText().toString());
        UserService.getInstance().login(this, user);
        send(true);
      }
    }

    if (view.getId() == R.id.tv_forgot) {
      startActivity(new Intent(this, ForgotActivity.class));
    }

    if (view.getId() == R.id.tv_signup) {
      startActivity(new Intent(this, SignUpActivity.class));
    }
  }

  @Override public void onSuccess(User user, String message) {
    UserUtil.getInstance(this).setLoginState(user, true);
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    startActivity(new Intent(this, HomeActivity.class));
    finish();
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    send(false);
  }

  private void send(boolean isSending) {
    email.setEnabled(!isSending);
    password.setEnabled(!isSending);
    btnMasuk.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorAccent);
    btnMasuk.setText(isSending ? "Tunggu Sebentar ..." : "Masuk");
    btnMasuk.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorAccent, null) : getResources().getColor(R.color.colorAccent)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }
}
