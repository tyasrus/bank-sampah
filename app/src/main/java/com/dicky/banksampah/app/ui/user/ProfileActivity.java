package com.dicky.banksampah.app.ui.user;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.Tempat;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.service.TempatService;
import com.dicky.banksampah.app.service.UserService;
import com.dicky.banksampah.app.ui.tempat.FormTempatActivity;
import com.dicky.banksampah.app.util.UserUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity
    implements UserService.ShowView, UserService.UpdateView, TempatService.GetView {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.name) EditText name;
  @BindView(R.id.textInputLayout2) TextInputLayout textInputLayout2;
  @BindView(R.id.email) EditText email;
  @BindView(R.id.textInputLayout3) TextInputLayout textInputLayout3;
  @BindView(R.id.password) EditText password;
  @BindView(R.id.textInputLayout4) TextInputLayout textInputLayout4;
  @BindView(R.id.phone) EditText phone;
  @BindView(R.id.btn_kirim) Button btnKirim;

  private Menu menu;
  private boolean isEdit = false;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_profile);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    User user = UserUtil.getInstance(this).getUser();
    UserService.getInstance().show(this, user);

    name.setTextColor(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
        R.color.textPrimary, null) : getResources().getColor(R.color.textPrimary));
    email.setTextColor(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
        R.color.textPrimary, null) : getResources().getColor(R.color.textPrimary));
    phone.setTextColor(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
        R.color.textPrimary, null) : getResources().getColor(R.color.textPrimary));

    configUi(true);
    Tempat tempat = new Tempat();
    tempat.setUser(new User(UserUtil.getInstance(this).getId()));
    TempatService.getInstance().get(this, tempat);
  }

  @OnClick(R.id.btn_kirim) public void onProfileButtonClick(View view) {
    menu.findItem(R.id.action_edit).setVisible(true);
    if (name.getText().toString().trim().length() == 0) {
      name.setError("Nama masih kosong");
    } else if (email.getText().toString().trim().length() == 0) {
      email.setError("Email masih kosong");
    } else if (password.getText().toString().trim().length() == 0) {
      password.setError("Password masih kosong");
    } else if (phone.getText().toString().trim().length() == 0) {
      phone.setError("Nomor Handphone masih kosong");
    } else {
      User user = new User();
      user.setId(UserUtil.getInstance(this).getId());
      user.setName(name.getText().toString());
      user.setEmail(email.getText().toString());
      user.setPassword(password.getText().toString());
      user.setPhone(phone.getText().toString());
      UserService.getInstance().update(this, user);
      send(true);
    }
  }

  @Override public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_profile, menu);
    this.menu = menu;
    if (UserUtil.getInstance(this).getUser().getLevel().getId() == 2) {
      menu.findItem(R.id.action_add_tempat).setVisible(false);
    }

    return super.onCreateOptionsMenu(menu);
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.action_edit) {
      User user = UserUtil.getInstance(this).getUser();
      UserService.getInstance().show(this, user);
      isEdit = true;
    }

    if (item.getItemId() == R.id.action_add_tempat) {
      startActivity(new Intent(this, FormTempatActivity.class));
    }

    if (item.getItemId() == android.R.id.home) {
      finish();
    }
    return super.onOptionsItemSelected(item);
  }

  private void configUi(boolean isFirstTime) {
    btnKirim.setVisibility(isFirstTime ? View.GONE : View.VISIBLE);
    password.setVisibility(isFirstTime ? View.GONE : View.VISIBLE);
    textInputLayout4.setVisibility(isFirstTime ? View.GONE : View.VISIBLE);

    name.setEnabled(!isFirstTime);
    email.setEnabled(!isFirstTime);
    phone.setEnabled(!isFirstTime);
  }

  @Override public void onSuccess(User user, String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    if (user.getId() == 0) {
      configUi(true);
    } else {
      if (isEdit) {
        menu.findItem(R.id.action_edit).setVisible(false);
        configUi(false);
      } else {
        name.setText(user.getName());
        email.setText(user.getEmail());
        phone.setText(user.getPhone());
        //password.setText(user.getPassword());
      }
    }
  }

  @Override public void onSuccess(Tempat tempat, String message) {
    menu.findItem(R.id.action_add_tempat).setVisible(false);
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    send(false);
  }

  private void send(boolean isSending) {
    name.setEnabled(!isSending);
    email.setEnabled(!isSending);
    password.setEnabled(!isSending);
    phone.setEnabled(!isSending);
    btnKirim.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorPrimary);
    btnKirim.setText(isSending ? "Tunggu Sebentar ..." : "Kirim");
    btnKirim.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorPrimary, null) : getResources().getColor(R.color.colorPrimary)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }
}
