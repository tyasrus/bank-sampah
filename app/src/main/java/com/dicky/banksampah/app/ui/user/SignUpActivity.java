package com.dicky.banksampah.app.ui.user;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.dicky.banksampah.app.R;
import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.entity.UserLevel;
import com.dicky.banksampah.app.service.UserLevelService;
import com.dicky.banksampah.app.service.UserService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity
    implements UserService.SignUpView, UserLevelService.AllView {

  @BindView(R.id.toolbar) Toolbar toolbar;
  @BindView(R.id.name) EditText name;
  @BindView(R.id.textInputLayout2) TextInputLayout textInputLayout2;
  @BindView(R.id.email) EditText email;
  @BindView(R.id.textInputLayout3) TextInputLayout textInputLayout3;
  @BindView(R.id.password) EditText password;
  @BindView(R.id.textInputLayout4) TextInputLayout textInputLayout4;
  @BindView(R.id.phone) EditText phone;
  @BindView(R.id.btn_daftar) Button btnDaftar;
  @BindView(R.id.user_level) Spinner userLevel;

  private List<UserLevel> levels;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_sign_up);
    ButterKnife.bind(this);
    setSupportActionBar(toolbar);
    levels = new ArrayList<>();
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) actionBar.setDisplayHomeAsUpEnabled(true);

    UserLevelService.getInstance().getAll(this);
  }

  @OnClick(R.id.btn_daftar) public void onSignUpButtonClick(View view) {
    if (name.getText().toString().trim().length() == 0) {
      name.setError("Nama masih kosong");
    } else if (email.getText().toString().trim().length() == 0) {
      email.setError("Email masih kosong");
    } else if (password.getText().toString().trim().length() == 0) {
      password.setError("Password masih kosong");
    } else if (phone.getText().toString().trim().length() == 0) {
      phone.setError("Nomor Handphone masih kosong");
    } else {
      User user = new User();
      user.setName(name.getText().toString());
      user.setEmail(email.getText().toString());
      user.setPassword(password.getText().toString());
      user.setPhone(phone.getText().toString());
      user.setLevel(new UserLevel(levels.get(userLevel.getSelectedItemPosition()).getId()));
      UserService.getInstance().signUp(this, user);
      send(true);
    }
  }

  @Override public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) finish();

    return super.onOptionsItemSelected(item);
  }

  @Override public void onSuccess(User user, String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    finish();
    send(false);
  }

  @Override public void onSuccess(List<UserLevel> userLevels, String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    this.levels.addAll(userLevels);
    List<String> strings = new ArrayList<>();
    for (UserLevel level : userLevels) {
      strings.add(level.getNama());
    }

    userLevel.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, strings));
  }

  @Override public void onFailed(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    send(false);
  }

  private void send(boolean isSending) {
    name.setEnabled(!isSending);
    email.setEnabled(!isSending);
    password.setEnabled(!isSending);
    phone.setEnabled(!isSending);
    btnDaftar.setBackgroundResource(isSending ? R.drawable.bg_button_loading : R.color.colorAccent);
    btnDaftar.setText(isSending ? "Tunggu Sebentar ..." : "Kirim");
    btnDaftar.setTextColor(
        isSending ? Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
            R.color.colorAccent, null) : getResources().getColor(R.color.colorAccent)
            : Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? getResources().getColor(
                android.R.color.white, null) : getResources().getColor(android.R.color.white));
  }
}
