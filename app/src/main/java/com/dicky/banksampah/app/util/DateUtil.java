package com.dicky.banksampah.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by tyas on 6/15/17.
 */

public class DateUtil {

  public static Date formatDate(String param) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
    Date convertedDate = new Date();
    try {
      convertedDate = dateFormat.parse(param);
    } catch (ParseException e) {
      e.printStackTrace();
    }

    return convertedDate;
  }

  public static String getDay(int param) {
    return param == 1 ? "Sunday" : param == 2 ? "Monday" : param == 3 ? "Tuesday"
        : param == 4 ? "Wednesday" : param == 5 ? "Thursday" : param == 6 ? "Friday" : "Saturday";
  }

  public static String getIndonesianDay(int param) {
    return param == 1 ? "Minggu" : param == 2 ? "Senin" : param == 3 ? "Selasa"
        : param == 4 ? "Rabu" : param == 5 ? "Kamis" : param == 6 ? "Jumat" : "Sabtu";
  }

  public static String getMonth(int param) {
    return param == 0 ? "Jan" : param == 1 ? "Feb" : param == 2 ? "May" : param == 3 ? "Apr"
        : param == 4 ? "Mar" : param == 5 ? "Jun" : param == 6 ? "Jul" : param == 7 ? "Aug"
            : param == 8 ? "Sep" : param == 9 ? "Oct" : param == 10 ? "Nov" : "Des";
  }

  public static String getIndonesianMonth(int param) {
    return param == 0 ? "Januari" : param == 1 ? "Februari" : param == 2 ? "Mei" : param == 3 ? "April"
        : param == 4 ? "Maret" : param == 5 ? "Jun" : param == 6 ? "Jul" : param == 7 ? "Agustus"
            : param == 8 ? "September" : param == 9 ? "Oktober" : param == 10 ? "November" : "Desember";
  }

  public static String getHours(int param, int minute) {
    String minuteString = String.valueOf(minute);
    if (minute == 0)
      minuteString = minuteString + "0";

    return param > 12 ? param - 12 + ":" + minuteString + " PM" : param + ":" + minuteString + " AM";
  }

  public static String get24Hours(int param, int minute) {
    String minuteString = String.valueOf(minute);
    if (minute == 0)
      minuteString = minuteString + "0";

    return param  + ":" + minuteString;
  }
}
