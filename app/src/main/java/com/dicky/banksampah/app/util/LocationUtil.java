package com.dicky.banksampah.app.util;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationUtil implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int LOCATION_REQUEST = 2;
    public static final String SEARCH_LOCATION_STATUS = "search_location_status";
    private Context context;
    private LocationManager manager;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private TrackingLocation trackingLocation;
    private SharedPreferences preferences;

    public LocationUtil(Context context) {
        this.context = context;
        manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        client = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(100); // Update location every second

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void connect() {
        client.connect();
    }

    public void setSearchLocationStatus(boolean isSearching) {
        preferences.edit().putBoolean(SEARCH_LOCATION_STATUS, isSearching).apply();
    }

    public boolean getSearchLocationStatus() {
        return preferences.getBoolean(SEARCH_LOCATION_STATUS, false);
    }

    public void find() {
        setSearchLocationStatus(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (context instanceof AppCompatActivity)
                ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_REQUEST);
        } else {
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if (trackingLocation != null)
                    trackingLocation.isGpsEnabled(manager.isProviderEnabled(LocationManager.GPS_PROVIDER));
            } else {
                if (client.isConnected())
                    LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, locationListener);
                else
                    connect();
            }
        }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        Toast.makeText(context, "Terjadi kesalahan, koneksi tidak stabil", Toast.LENGTH_SHORT).show();
        client.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(context, "Terjadi kesalahan saat mencari lokasi", Toast.LENGTH_SHORT).show();
    }

    public void setTrackingLocation(TrackingLocation trackingLocation) {
        this.trackingLocation = trackingLocation;
    }

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                if (trackingLocation != null)
                    trackingLocation.onLocationFound(location);

                LocationServices.FusedLocationApi.removeLocationUpdates(client, this);
            } else {
                find();
            }
        }
    };

    public interface TrackingLocation {
        void onLocationFound(Location location);

        void onLocationError(String message);

        void isGpsEnabled(boolean isEnabled);
    }
}
