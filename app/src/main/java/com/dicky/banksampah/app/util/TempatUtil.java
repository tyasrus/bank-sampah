package com.dicky.banksampah.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dicky.banksampah.app.entity.User;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tyas on 9/30/17.
 */

public class TempatUtil {

    public static final String NAME = "name";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    private static SharedPreferences sharedPreferences;

    public static TempatUtil getInstance(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return new TempatUtil();
    }

    public void setName(String name) {
        sharedPreferences.edit().putString(NAME, name).apply();
    }

    public String getName() {
        return sharedPreferences.getString(NAME, "");
    }

    public void setLatLng(double latitude, double longitude) {
        sharedPreferences.edit().putString(LATITUDE, latitude == 0 ? "" : String.valueOf(latitude)).apply();
        sharedPreferences.edit().putString(LONGITUDE, longitude == 0 ? "" : String.valueOf(longitude)).apply();
    }

    public LatLng getLatLng() {
        LatLng latLng = null;
        if (!sharedPreferences.getString(LATITUDE, "").equals("") && !sharedPreferences.getString(LONGITUDE, "").equals("")) {
            latLng = new LatLng(Double.parseDouble(sharedPreferences.getString(LATITUDE, "")),
                    Double.parseDouble(sharedPreferences.getString(LONGITUDE, "")));
        }

        return latLng;
    }
}
