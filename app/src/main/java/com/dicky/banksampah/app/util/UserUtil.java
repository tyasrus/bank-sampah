package com.dicky.banksampah.app.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dicky.banksampah.app.entity.User;
import com.dicky.banksampah.app.entity.UserLevel;

import org.json.JSONException;
import org.json.JSONObject;

public class UserUtil {

    public static final String ID_USER = "id_user";
    public static final String USER = "user";
    public static final String IS_LOGIN = "is_login";

    private static SharedPreferences sharedPreferences;

    public static UserUtil getInstance(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return new UserUtil();
    }

    public void setLoginState(User user, boolean isLogin) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", user.getId());
            jsonObject.put("name", user.getName());
            jsonObject.put("email", user.getEmail());
            jsonObject.put("phone", user.getPhone());
            jsonObject.put("id_user_level", user.getLevel().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sharedPreferences.edit().putInt(ID_USER, user.getId()).apply();
        sharedPreferences.edit().putString(USER, jsonObject.toString()).apply();
        sharedPreferences.edit().putBoolean(IS_LOGIN, isLogin).apply();
    }

    public int getId() {
        return sharedPreferences.getInt(ID_USER, 0);
    }

    public boolean isLogin() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public User getUser() {
        User user = new User();
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferences.getString(USER, ""));
            user.setId(jsonObject.getInt("id"));
            user.setLevel(new UserLevel(jsonObject.getInt("id_user_level")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    public void logout() {
        sharedPreferences.edit().putString(USER, null).apply();
        sharedPreferences.edit().putString(ID_USER, null).apply();
        sharedPreferences.edit().putBoolean(IS_LOGIN, false).apply();
    }
}
